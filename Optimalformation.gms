*Notes before solving the problem
*In this problem we want to find the formation that maximixze the total fitness player-role.
* In order to do so there are some constrians that should be forfilled. We can write the problem as the
*following (the mathematical programming problem is in the last part of the code):

* Object function: maximize the overall fitness player role
* s.t.
* 1) Only one formatin is choosen
* 2) each player can at most be enrolled in one role
* 3) When a formation is choosen we have to make sure that each role in the choosen formation has a player assigned
* 4) We need to have at least one quality player
* 5) If all the quality players have been choosen then we need at least one strenght player

*The problem implemented in GAMS:

*First we define the varibles in the problem:
Sets
p the players /p1*p25/
pq(p) subset of player (quality) /p13,p20, p21, p22/
ps(p) subset of player (strength) /p10, p12, p23/
f the possible formations / 442 , 352, 4312, 433, 343, 4321/
r the possible roles / GK, CDF, LB, RB, CMF, LW, RW, OMF, CFW, SFW/
;

*The folloning table shows each of the formations and how many players should be assigned each role for the given formation:

Table PR(f,r) the number of players requred for each formation and role
    GK  CDF LB  RB  CMF LW  RW  OMF CFW SFW
442 1   2   1   1   2   1   1   0   2   0
352 1   3   0   0   3   1   1   0   1   1
4312 1   2   1   1   3   0   0   1   2   0    
433 1   2   1   1   3   0   0   0   1   2
343 1   3   0   0   2   1   1   0   1   2
4321 1   2   1   1   3   0   0   2   1   0
;

*The following table shows all the players and their fitness number for each role:

Table m(p,r) the fitness player role
    GK  CDF LB  RB  CMF LW  RW  OMF CFW SFW
P1  10  0   0   0   0   0   0   0   0   0
P2  9   0   0   0   0   0   0   0   0   0
P3  8.5 0   0   0   0   0   0   0   0   0
P4  0   8   6   5   4   2   2   0   0   0
P5  0   9   7   3   2   0   2   0   0   0
P6  0   8   7   7   3   2   2   0   0   0
P7  0   6   8   8   0   6   6   0   0   0
P8  0   4   5   9   0   6   6   0   0   0
P9  0   5   9   4   0   7   2   0   0   0
P10 0   4   2   2   9   2   2   0   0   0
P11 0   3   1   1   8   1   1   4   0   0
P12 0   3   0   2  10   1   1   0   0   0
P13 0   0   0   0   7   0   0   10  6   0
P14 0   0   0   0   4   8   6   5   0   0
P15 0   0   0   0   4   6   9   6   0   0
P16 0   0   0   0   0   7   3   0   0   0
P17 0   0   0   0   3   0   9   0   0   0
P18 0   0   0   0   0   0   0   6   9   6
P19 0   0   0   0   0   0   0   5   8   7
P20 0   0   0   0   0   0   0   4   4   10
P21 0   0   0   0   0   0   0   3   9   9
P22 0   0   0   0   0   0   0   0   8   8
P23 0   3   1   1   8   4   3   5   0   0
P24 0   3   2   4   7   6   5   6   4   0
P25 0   4   2   2   6   7   5   2   2   0
;


*We define the decisions varibles:

Variable
z the overall fitness value we want to maximze
x(p,r) this value is 1 if player p is assinged to role r
y(f) this varible is one if formation f is the choosen formation
;

*Defining the type of the varibles:

binary Variable x;
binary variable y;

*We write down the obejct function and the constrians: 

Equation
opt    define objective function
formation we choose only one formation
player(p) limit on how many roles a player can take
formarionr(r) there is a limit on how many players we should use for each role given the choosen formation. 
playerq limit on how many qulity players to choose
players limit on how many strengh player to choose
;

*We write the programming problem:

opt..               z =e= sum(p,sum(r, m(p,r)*x(p,r)));
formation..         sum(f,y(f)) =e= 1;
player(p)..         sum(r,x(p,r)) =l= 1;
formarionr(r)..     sum(p, x(p,r)) =e= sum(f,PR(f,r)*y(f)); 
playerq..           sum(pq,sum(r, x(pq,r))) =g= 1;
players..           sum(pq,sum(r,x(pq,r))) =l= sum(ps,sum(r,x(ps,r))) + card(pq) -1;

*We say which constrains that should be in the model:

Model copenhagen / all /;

* We solve the model using mixed integer problem and we want to maximize our object function:

solve copenhagen using mip MAXIMIZING z;

* We show the result for the defined varibles

Display z.l, x.l, y.l;

*Optimal object function value: 97

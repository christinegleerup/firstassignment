$eolcom//
*Notes before solving the problem
* In this problem we have to plan the sailing activities for a company in order to minimize the total cost. Doing so
* we have have some constrians that most be forfilled. We can write the problem as the
*following (the mathematical programming problem is seen in the last part of the code):

* Object function: minimize the cost of the sailing activities (the cost includes both the fixed cost when using a 
*ship and cost for each chosen route)
* s.t.
* 1) There is a limit on how many ports we should visit at least
* 2) THIS CONSTRAIN IS CHANGED: There is a limit on how many days each ship can sail a year
* 3) If a port is visit there is a minimum of time this port should be visit every year
* 4) Certain ports are incompatible with each other, only one of them can be part of the saling plan
* 5) Certain ports are incompatible with each other, only one of them can be part of the saling plan
* 6) THIS CONSTRAIN IS DELETETED AND COMBINED WITH CONSTRAIN 2: If a ship has been used we force the varible y(v) to be 1


*The problem implemented in GAMS:

*First we define the varibles in the problem:

Sets
v ships /v1*v5/
r routes /Asia,ChinaPacific/
p ports / Singapore, Incheon, Shanghai, Sydney, Gladstone, Dalian, Osaka, Victoria/
h1(p) subset of ports /Singapore, Osaka/
h2(p) subset of ports /Incheon, Victoria/
;

Scalar
K ports to service /5/
;

Parameters
f(v) the fixed cost of using ship v /v1 65,v2 60,v3 92,v4 100,v5 110/
g(v) amount of days ship v sails a year /v1 300,v2 250,v3 350,v4 330,v5 300/
d(p) times to vist a port if the port is choosen
/Singapore 15, Incheon 18, Shanghai 32, Sydney 32, Gladstone 45, Dalian 32, Osaka 15, Victoria 18/
;

*This table shows the cost for each ship to sail on each route

Table C(v,r) cost for ship v on route r
    Asia    ChinaPacific
v1  1.41    1.9
v2  3       1.5
v3  0.4     0.8
v4  0.5     0.7
v5  0.7     0.8
;

* This table shows how many day it takes each ship to complete each route

Table T(v,r) Number of dates neede to complete one route
    Asia    ChinaPacific
v1  14.4    21.2
v2  13.0    20.7
v3  14.4    20.6
v4  13.0    19.2
v5  12.0    20.1
;

*This table shows which ports each route includes

Table A(p,r) which ports route r passes though 
            Asia    ChinaPacific
Singapore   1       0
Incheon     1       0
Shanghai    1       1
Sydney      0       1
Gladstone   0       1
Dalian      1       1
Osaka       1       0
Victoria    0       1
;


*We define the decisions varibles:

Variables
w the cost the firm wants to minimize
x(v,r) counts the times ship v takes route r 
u(p) binary varible, the varible is 1 if the port has been visited otherwise 0
y(v) is one if ship v is used
;

*Defining the type of the varibles:

Integer variable x;
binary variable u;
binary variable y;

*We write down the obejct function and the constrians: 

Equations
cost the cost we want to minimize
constrian1 the number of ports we should visit at least 
constrian2(v) *NEW CONSTRAIN: the maksimum of how many days a ship can sail a year for the choosen ships
constrian3(p) times we have to visti a port if that port is choosen
constrian4 certain ports are incompatible with each other
constrian5 certain ports are incompatible with each other
;


*We write the programming problem:

cost..              w =e= sum((v,r),x(v,r)*C(v,r))+ sum(v,y(v)*f(v));
constrian1..        sum(p,u(p)) =g= K;
constrian2(v)..     sum(r,x(v,r)*T(v,r)) =l= g(v)*y(v); //Changed
constrian3(p)..     sum((v,r),x(v,r)*A(p,r)) =g= d(p)*u(p);
constrian4..        sum(h1,u(h1)) =l= 1;
constrian5..        sum(h2,u(h2)) =l= 1;


*We say which constrains that should be in the model:

Model fleet / all /;

* We solve the model using mixed integer problem and we want to minimize our object function:

solve fleet using mip minimizing w;

* We show the result for the defined varibles

Display w.l, x.l, u.l, y.l;

*Optimal object function value: 272.4

$eolcom//
*Notes before solving the problem
* In this problem we want to distribute keys to nodes in a network system in order to maximize the number
*of direct connections. In order to do so there are some constrians that should be forfilled.
*We can write the problem as the following (the mathematical programming problem is in the last part of the code):

* Object function: maximize the overall number of direct connections
* s.t.
* 1) Each node have a limit of space to store keys
* 2) There is a limit on how many times each key can be used
* 3) *NEW CONSTRAIN: If two nodes have a key in common we force a binary varible to be 1
* 4) *NEW CONSTRAIN: If two nodes don't have a key in commen we force a binary varible to be 0
* 5) *NEW CONSTRAIN: If two nodes don't have three keys in common we force a binary varible to be 0 

*The problem implemented in GAMS:

*First we define the varibles in the problem:
Sets
k keys /k1*k30/
i nodes /i1*i8/
alias(i,j);
*copying the set of nodes

Scalar
mk the space that each key obtain in a node /186/
mi the available space in each node /1000/
t times each key can be used /2/
;

*We define the decisions varibles:

Variables
d the object fuctions value
x(i,k) this varible is 1 if key k is assigned to node i
y(i,j,k) this varible is 1 if node i and j have key k in common
z(i,j) this varible is 1 if node i and j have three keys in common
;

* As described above the following varibles are bianry

binary variable x;
binary variable y;
binary Variable z;

*We write down the obejct function and the constrians: 

Equation
opt define objective function

capasiti(i)
*limit of space for each node

keyDistribution(k)
*limt of times we can use a key

constrain1(i,j,k)
*NEW CONSTRAIN: This constrian makes sure that y(i,j,k) becomes 1 when node i and j have a key in common

constrain2(i,j,k)
*NEW CONSTRAIN: This constrian makes sure that y(i,j,k) is 0 when node i and j don't have a key in common

constrain3(i,j)
*NEW CONSTRAIN: If node i and j have less than three keys in commen then the variable z(i,j) is forced to be 0.
* We don't need a constrain that force z(i,j) to be 1 when node i and j have 3 keys in common because we are maksimizing our obejct function
* hence choosen z(i,j) as large as possible. A constrain that force z(i,j) to be 1 when x(i,k) and x(j,k) have
*three keys in common would look like: 3 =g= sum(k,y(i,j,k))+ 1 - z(i,j)*M , where M takes the value of the 
*maksium number of keys two nodes can share minus 2. Then z(i,j) becomes 1 as soon as two nodes have at least tree keys in common.
;

*We write the programming problem:

opt..                                  d =e= sum((i,j)$[ord(i)<ord(j)],z(i,j));
capasiti(i)..                          sum(k, x(i,k)*mk) =l= mi;
keyDistribution(k)..                   sum(i,x(i,k)) =l= t; 
constrain1(i,j,k)$[ord(i)<ord(j)]..    1 + y(i,j,k) =g= (x(i,k) + x(j,k)); //Changed
constrain2(i,j,k)$[ord(i)<ord(j)]..    (x(i,k) + x(j,k)) =g= 2*y(i,j,k); //Changed
constrain3(i,j)$[ord(i)<ord(j)]..      sum(k,y(i,j,k)) =g= 3*z(i,j); //Changed
                                        
*Note that we use $[ord(i)<ord(j)] to make sure node i is not equal to node j and
* to avoid looking at both z(i,j) and z(j,i) which reprecent the same connection and should therefore not be
* counted twice. 

*We say which constrains that should be in the model:

Model network / all /;

* We solve the model using mixed integer problem and we want to maximize our object function:
solve network using mip MAXIMIZING d;

* We show the result for the defined varibles 
Display d.l, x.l, y.l, z.l;

*Optimal object function value: 4.
